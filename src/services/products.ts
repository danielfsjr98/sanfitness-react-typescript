import { IProduct, ICreateProduct } from "../context/shop/interfaces";
import api from "./api";

const services = {
  createProducts: async (payload: ICreateProduct) => {
    return await api.post("/products", payload);
  },
  getAvailableProductsShop: async () => {
    return await api.get("/oneProductsByRef");
  },
  getProductByRef: async (ref: string) => {
    return await api.get(`/productsByRef/${ref}`);
  },
};

export default services