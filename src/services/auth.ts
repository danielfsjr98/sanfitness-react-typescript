import api from "./api";
import { IUser, ILogin } from "../context/auth/interfaces";

const services = {
  setUser: (payload: IUser ) => {
    const user = payload
    if(user) {
        api.defaults.headers.common['Authorization'] = `bearer ${user.token}`
    } else {
        delete api.defaults.headers.common['Authorization']
    }
  },
  signin: async (payload: ILogin ) => {
    return await api.post("/signin", payload);
  },
  setValidateToken: async (payload: String) => {
    return await api.post('/validateToken', payload)
  },
};

export default services