import React from "react";
import {
  BrowserRouter as Router,
} from "react-router-dom";
import styled from "styled-components";
import { AuthProvider } from "./context/auth"
import { ErrorProvider } from "./context/error";
import Routes from './routes'

const AppLayout = styled.div`
  min-height: 100vh;
`;

const App: React.FC = () => (
  <Router>
    <ErrorProvider>
      <AuthProvider>
        <AppLayout>
          <Routes />
        </AppLayout>
      </AuthProvider>
    </ErrorProvider>
  </Router>
);

export default App;
