import Admin from "../views/admin"
import { IRoutes } from "./index";

const routes: IRoutes[] = [
  {
    path: "/admin",
    name: "admin",
    component: Admin,
  }
];

export default routes;
