import SignIn from "../views/SignIn";
import { IRoutes } from "./index";

const routes: IRoutes[] = [
  {
    path: "/login",
    name: "login",
    component: SignIn,
  }
];

export default routes;
