import React, { useEffect } from "react";
import { Switch, Redirect, Route } from "react-router-dom";
import publicRoutes from "./public.routes";
import privateRoutes from "./private.routes";
import adminRoutes from "./admin.routes";
import { useAuth } from "../context/auth/index";
import { ShopProvider } from "../context/shop";
import Content from '../components/template/Content'
// import {
//   userRoutes,
//   customerRoutes,
//   profileRoutes,
//   paymentRouter,
//   graphicalAccountRoutes,
// } from "./private.routes";

export interface IRoutes {
  path: string;
  name: string;
  component: React.FC;
}

const Routes = () => {
    const { signed, validateSession, userLogged } = useAuth();

    useEffect(() => {
      validateSession();
    }, [signed]);

    return (
    <Switch>
      <Content>
        <Route exact path="/">
          {signed ? <Redirect to="/home" /> : <Redirect to="/login" />}
        </Route>
        {publicRoutes.map((route) => (
          <Route key={route.name} path={route.path} component={route.component} />
        ))}
        <ShopProvider>
          { !!userLogged.id && privateRoutes.map((route) => (
            <Route
              key={route.name}
              path={route.path}
              component={route.component}
              exact
            /> 
          )) }
          { userLogged.admin && adminRoutes.map((route) => (
            <Route
              key={route.name}
              path={route.path}
              component={route.component}
              exact
            > 
            </Route>
          )) }
          {/* Ver questão de rotas inexistentes */}
          {/* <Redirect from="*" to="/home" /> */}
        </ShopProvider>
      </Content>
    </Switch>
  );
};
export default Routes;
