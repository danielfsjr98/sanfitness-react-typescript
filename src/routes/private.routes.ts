import Home from "../views/Home";
import Cart from "../views/Contact";
import Products from "../views/shop/Products";
import ProductByRef from "../views/shop/ProductByRef"
import { IRoutes } from "./index";

const routes: IRoutes[] = [
  {
    path: "/home",
    name: "home",
    component: Home,
  },
  {
    path: "/products",
    name: "products",
    component: Products,
  },
  {
    path: "/product/:ref",
    name: "productByRef",
    component: ProductByRef,
  },
  {
    path: "/cart",
    name: "cart",
    component: Cart,
  }
];

export default routes;
