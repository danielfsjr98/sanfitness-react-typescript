import styled from "styled-components";
import React from "react";
import { useHistory } from "react-router-dom";
import { IProduct } from "../../context/shop/interfaces"
import { formatCurrency } from "../../utils"
interface ICardProduct {
    product: IProduct,
};

const CardStyle = styled.div`
    display: flex;
    flex-direction: column;
    color: white;
    background-color: #fff;
    color: #232526;
    border-radius: 5px;
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
`;

const CardHeader = styled.div`
    display: flex;
    width: 100%;
    min-height: 70%;
    flex-direction: column;
    background-color: #232526;
    color: white;
    img{
        display: flex;
        width: 100%;
        height: 85%;
    }
    h2{
        margin: auto 6px;
        font-size: 16px;
        font-weight: normal;
    }
`

const CardContent = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    padding: 5px 6px;
`
const Button = styled.button`
    margin-top: 5px;
    border: none;
    background-color: #232526;
    color: white;
    padding: 12px; 
    border-radius: 5px;
    cursor: pointer;
    outline: none;
    :hover{
        background-color: #ffb8b8;
    }
`

const Contact: React.FC<ICardProduct> = (props) => {
    const history = useHistory();
    const { product } = props

    const goToProduct = (ref: string) => {
        history.push(`product/${ref}`);
    }
    
    return(
        <CardStyle>
            <CardHeader>
                <img src={product.imageUrl} alt={product.name} />
                <h2>{ product.name }</h2>
            </CardHeader>
            <CardContent>
                <div>
                    Preço: <strong>{ formatCurrency(product.price) }</strong>
                </div>
                <div>
                    { product.description.length >= 40 ? 
                    `${product.description.substring(0, 40)}...` : product.description }
                </div>    
                <Button onClick={ _ => goToProduct(product.ref) }>Ver mais</Button>
            </CardContent>
        </CardStyle>
    )

};

export default Contact;