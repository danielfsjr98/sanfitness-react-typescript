import styled from "styled-components";
import React from "react";

interface IInput {
    label: string;
    value?: string | number;
    placeholder?: string;
    type?: string;
    name?: string;
    onChange?: (event: React.FormEvent<HTMLInputElement>) => void;
    padding?: string;
}

const InputContainer = styled.div`
    min-height: 50px;
    width: 100%;
    box-sizing: border-box;
`;

const InputStyle = styled.input`
    width: 100%;
    outline: none;
    background-color: #3d3d3d;
    padding: ${(props: IInput) => props.padding ? props.padding : '10px 5px' };
    border: none;
    border-radius: 3px;
    color: white;
    font-size: 16px;
`;

const LabelInputStyle = styled.label`
    font-size: 0.8rem;
`;

const Input: React.FC<IInput> = (props) => {
    const { label } = props

    return(
        <InputContainer>
            <LabelInputStyle>{ label }</LabelInputStyle> <br />
            <InputStyle {...props} /> 
        </InputContainer>
    )};
  export default Input;