import styled from "styled-components";
import React from "react";

interface ITable {
    header: string[];
    items: any[];
    action?: boolean;
    onClick?: (id: string) => void;
    keyOnClick?: any;
}

const TableStyle = styled.table`
    width: 100%;
    text-align: center;
    padding: 15px 3px;
    border: 1px solid;
    border-radius: 3px;
    border-collapse: collapse;
`;

const ButtonEdit = styled.button`
    padding: 5px 30px;
`;

const Table: React.FC<ITable> = (props) => {
    const { header, items, action, onClick, keyOnClick } = props
    return(
        <TableStyle>
            <thead style={{ backgroundColor: "#3d3d3d", color: "white" }}>
                <tr>
                    { header && header.map((item, index) => 
                        <th key={index} style={{ padding: "10px"}}>
                            { item }
                        </th>
                    )}
                    { action && <th>Ação</th> }
                </tr>
            </thead>
            <tbody>
                { items && items.map((item, index) => 
                    <tr key={index}>
                        { Object.keys(item).map((key, id) => <td key={id}> {item[key]} </td>) }
                        { (action && keyOnClick) && 
                            <td> 
                                <ButtonEdit onClick={ _ => !!onClick && onClick(item[keyOnClick]) }> Ver </ButtonEdit> 
                            </td>
                        }
                    </tr>
                )}        
            </tbody>
        </TableStyle>
    )
};

export default Table;