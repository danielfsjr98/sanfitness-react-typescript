import styled from "styled-components";
import React from "react";

interface IOption {
    name: string;
    id: string | number;
}

interface ISelect {
    label: string;
    value?: string;
    placeholder?: string;
    type?: string;
    name?: string;
    items: IOption[];
    onChange?: (event: React.FormEvent<HTMLInputElement>) => void;
    setSelectedValue?: React.Dispatch<React.SetStateAction<IOption>>;
}

const InputContainer = styled.div`
    min-height: 50px;
    width: 100%;
    box-sizing: border-box;
    margin-top: 8px;
`;

const SelectStyle = styled.select`
    margin-bottom: 10px;
    width: 100%;
    padding: 5px;
    outline: none;
    background-color: #3d3d3d;
    padding: 10px 5px;
    border: none;
    border-radius: 3px;
    color: white;
    font-size: 16px;
`;

const LabelInputStyle = styled.label`
    font-size: 0.8rem;
`;

const Input: React.FC<ISelect> = (props) => {
    const { label, items, setSelectedValue } = props

    const onChange = (event: React.ChangeEvent<HTMLSelectElement>) =>{
        const { value } = event.currentTarget
        const selected = items.find(item => item.id === parseInt(value))
        if(setSelectedValue && selected) setSelectedValue(selected)
    }

    return(
        <InputContainer>
            <LabelInputStyle>{ label }</LabelInputStyle> <br />
            <SelectStyle name="select" onChange={onChange}>
                { items.map((item) => 
                    <option key={item.id} value={item.id} >
                        { item.name }
                    </option>
                )}
            </SelectStyle>
        </InputContainer>
    )};
  export default Input;