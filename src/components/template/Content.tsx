import styled from "styled-components";
import React, { useState } from "react";
import Menu from "../template/Menu"
import { useAuth } from "../../context/auth/index";
import Sidebar from "./Sidebar";
import { useErrors } from "../../context/error";
import Toast from "../toast/Toast";
import { CurrentBreakpoint } from "../../utils"

type ContentProps = {
    signed?: boolean;
    toggle?: boolean;
};

const ContentStyle = styled.div`
    min-height: 100vh;
    display: grid;
    grid-template-rows: ${(props: ContentProps) => props.signed ? '60px 1fr' : '100%'};
    grid-template-columns: ${(props: ContentProps) => props.signed ? '225px 1fr' : '100%'};
    grid-template-areas: ${(props: ContentProps) => props.signed ? "'side menu' 'side content'" : "'content content'"} ;
    background-color: #e9e9e9;

    @media(max-width: 768px){
        grid-template-columns: ${(props: ContentProps) => props.toggle ? '175px 1fr' : '100%' };
        grid-template-areas: ${(props: ContentProps) => props.signed ? 
        (props.toggle ? "'side menu' 'side content'" : "'menu' 'content'") :  "'content content'"};
    }
`;

const ContentArea = styled.div`
    grid-area: content;
    display: flex;
    padding: 15px;
    @media(max-width: 768px){
      padding: 5px;
    }
`
const Content: React.FC<ContentProps> = ({children}) => {
    const { signed } = useAuth();
    const { showToast, themeToast, messageToast } = useErrors();
    const [toggle, setToggle] = useState(true);

    return(
        <ContentStyle signed={ signed } toggle={toggle}>  
            { showToast && (
                <Toast top={"85"} theme={themeToast} message={messageToast} />
            )}
            { ((CurrentBreakpoint() !== 'mobile' || toggle  ) && signed) && 
                <Sidebar /> 
            }
            { signed && <Menu toggle={toggle} setToggle={setToggle}/> }
            <ContentArea>
                {children}
            </ContentArea>
        </ContentStyle>
    )
};
    
export default Content;