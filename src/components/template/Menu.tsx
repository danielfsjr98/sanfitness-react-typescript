import styled from "styled-components";
import React from "react";
import MenuDrop from "../menuDrop/Index"
import HamburgerIcon from "../../assets/hamburger-icon.svg"
import { CurrentBreakpoint } from "../../utils"

interface IMenu {
  toggle: boolean;
  setToggle?: React.Dispatch<React.SetStateAction<boolean>>;
}

const MenuStyle = styled.div`
  grid-area: menu;
  display: grid;
  background-color: #ffb8b8;
  max-width: 100%;
  grid-template-columns: 50px 1fr 100px;
  grid-template-rows: 100%;
  grid-template-areas: "toggle title menuDrop";
  @media(max-width: 768px){
      grid-template-columns: ${(props: IMenu) => props.toggle ? '50px 1fr' : '50px 1fr 70px'};
      grid-template-areas: ${(props: IMenu) => props.toggle ? 'toggle menuDrop' : 'toggle title menuDrop'};
  }
`;

const TitleStyle = styled.div`
  grid-area: title;
  display: flex;
  justify-content: center;
  height: 100%;
  align-items: center;
  h3{
    color: white;
    font-weight: 550;
    font-size: 24px;
  }
`

const ToggleMenu = styled.div`
  grid-area: toggle;
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 5px;
  cursor: pointer;
`
const Menu: React.FC<IMenu> = (props) => {
    const {toggle, setToggle } = props
    const toggleMenu = () => {
      !!setToggle && setToggle(!toggle)
    }

    return(
        <MenuStyle toggle={toggle}>
            { CurrentBreakpoint() === "mobile" && 
              <ToggleMenu onClick={toggleMenu}>
                <img src={HamburgerIcon} alt="menu"/>
              </ToggleMenu>
            }
            { (CurrentBreakpoint() !== "mobile" || !toggle) &&
              <TitleStyle>
                <h3>Sanfitness - Revenda</h3>
              </TitleStyle>
            }
            <MenuDrop />
        </MenuStyle>
    )
};
export default Menu;