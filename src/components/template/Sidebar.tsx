import styled from "styled-components";
import React from "react";
import { NavLink } from 'react-router-dom'
import LogoMenu from '../../assets/logo_menu.png'

const SidebarStyle = styled.div`
  grid-area: side;
  background: linear-gradient(to right, #232526, #414345);
  display: flex;
  align-items: center;
  padding-top: 20px;
  flex-direction: column;
  height: 100%;
  width: 100%;
  a {

    display: flex;
    height: 50px;
    width: 100%;
    align-items: center;
    padding: 10px;
    padding-left: 35px;
    text-decoration: none;
    color: white;
    :hover{
      background-color: rgba(0, 0, 0, 0.25);
    }
  }
`;

const activeClassName = 'nav-item-active'

const StyledLink = styled(NavLink).attrs({ activeClassName })`
  &.${activeClassName} {
    background-color: rgba(0, 0, 0, 0.25);
  }
`;

const LogoImg = styled.img`
  max-width: 150px;
  max-height: 150px;
`;

const Sidebar: React.FC = (props) => {
    return(
        <SidebarStyle>
            <LogoImg src={LogoMenu}></LogoImg>
            <StyledLink to="/home" exact>Home</StyledLink>
            <StyledLink to="/products" exact>Catálogo</StyledLink>
            <StyledLink to="/mySales" exact>Meus pedidos</StyledLink>
            <StyledLink to="/cart" exact>Carrinho</StyledLink>
        </SidebarStyle>
    )};
  export default Sidebar;