import styled from "styled-components";
import React from "react";

interface ICheckbox {
    label: string;
    checked: boolean;
    placeholder?: string;
    type?: string;
    name?: string;
    id: string | number;
    onChange?: (event: React.FormEvent<HTMLInputElement>) => void;
}

const Container = styled.div`
    display: flex;    
    align-items: center;
    margin: 6px 0;
`;
const CheckboxContent = styled.div`
    display: flex;
    margin-right: 6px;
`;

const CheckboxStyle = styled.input.attrs({ type: 'checkbox' })`
    width: 15px;
    height: 15px;
`;

const LabelCheckboxStyle = styled.div`
    display: flex;
    font-size: 0.8rem;
`;

const Checkbox: React.FC<ICheckbox> = (props) => {
    const { label, checked, onChange, id } = props
    return(
        <Container>
            <CheckboxContent>
                <CheckboxStyle checked={checked} onChange={onChange} id={id.toString()} /> 
            </CheckboxContent>
            <LabelCheckboxStyle>{ label }</LabelCheckboxStyle> 
        </Container>
    )};
  export default Checkbox;