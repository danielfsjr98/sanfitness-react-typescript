import styled from "styled-components";
import React from "react";

interface IForm {
    title: string;
    textButton?: string;
    onSubmit: (event: React.FormEvent) => Promise<void>;
}

const FormStyle = styled.form`
  display: flex;
  flex-direction: column;

  h1{
    font-weight: normal;
    margin-bottom: 5px;
  }
`;

const Form: React.FC<IForm> = ({children, ...props}) => {

    return(
        <FormStyle {...props}>
            { children }
        </FormStyle>
    )};
export default Form;