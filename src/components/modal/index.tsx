import React from "react";
import styled from "styled-components";

interface LogoProps {
  locale?: string;
}
const Modal = styled.div `
  position: fixed;
  display: flex;
  align-items: flex-start;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: rgba(0,0,0,0.3);
  z-index: 9999;
  opacity:1;
  -webkit-transition: opacity 400ms ease-in;
  -moz-transition: opacity 400ms ease-in;
  transition: opacity 400ms ease-in;
  overflow-y: auto;
  ::-webkit-scrollbar-thumb {
    background-color: red;
    outline: 1px solid slategrey;
  }
  &:target {
    opacity: 1;
  }
  @media(max-width: 800px){
    position: fixed;
    width: 100%;
    height: 100vh;
    padding: 30px;
    justify-content: center;
  }
`

const Container = styled.div`
  position: relative;
  z-index:99999;
  padding:12px;
  left: 40%;
  width: 370px;
  align-self: center;
  background: #FFFFFF;
  box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.15);
  border-radius: 5px;
  overflow-y: auto;
  max-height: 100%;
  

  @media(max-width: 800px){
    padding: 20px;
    left: 0;
    position: relative;
    width: 100%;
    max-height: 100%;
  }
  

`
const Logo: React.FC<LogoProps> = ({ children, ...props }) => {
    return (
        <Modal>
            <Container {...props}>
                {children}
            </Container>
        </Modal>
    )
};

export default Logo;
