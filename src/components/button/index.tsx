import styled from "styled-components";
import React from "react";

interface IButton {
    text: string;
    color?: string;
    background?: string;
    onClick?: () => void;
};

const ButtonStyle = styled.button`
    outline: none;
    cursor: pointer;
    padding: 12px 3px;
    background-color: ${(props: IButton) => props.background ? props.background : '#fff'};
    border: 1px solid;
    border-color: ${(props: IButton) => props.color ? props.color : '#000'};
    border-radius: 2px;
    font-size: 14px;
    font-weight: bold;
    color: ${(props: IButton) => props.color ? props.color : '#000'};
    width: 100%;
`;


const Contact: React.FC<IButton> = (props) => {
    const { text } = props
    
    return(
        <ButtonStyle {...props}>
            { text.toUpperCase() }
        </ButtonStyle>
    )

};

export default Contact;