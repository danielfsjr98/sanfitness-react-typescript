import styled from "styled-components";
import React from "react";
import { useAuth } from "../../context/auth"
import { NavLink } from 'react-router-dom'
const MenuDropStyle = styled.div`
  grid-area: menuDrop;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const ButtonStyle = styled.button`
  border: none;
  outline: none;
  cursor: pointer;
  color: white;
  border-radius: 3px;
  background-color: #ffb8b8;
  font-size: 14px;
`;
const activeClassName = 'nav-item-active'

const StyledLink = styled(NavLink).attrs({ activeClassName })`
  color: white;
  &.${activeClassName} {
    background-color: rgba(0, 0, 0, 0.25);
  }
`;
const MenuDrop: React.FC = (props) => {
    const { signout, userLogged }= useAuth()
    return(
        <MenuDropStyle>
            <ButtonStyle onClick={() => signout()}>Logout</ButtonStyle>
            { userLogged.admin && 
              <StyledLink to="/admin">Admin</StyledLink>
            }
        </MenuDropStyle>
    )};
  export default MenuDrop;