import React from "react";
import styled from "styled-components";
import SuccessImg from "../../assets/correct.png";
import ErroImg from "../../assets/erro.png";
import { useErrors } from "../../context/error";
import CloseErrorPng from '../../assets/close_error.png'
import CloseSuccessPng from '../../assets/close_success.png'

interface IToast {
  message?: string;
  theme: string;
  top: string;
  setToastError?: React.Dispatch<React.SetStateAction<boolean>>;
}

const ContainerToast = styled.div`
  background: ${({ theme }) =>
    theme === "success" ? "rgb(215,235,231)" : "#E4A398"};
  display: flex;
  align-self: center;
  justify-content: center;
  align-items: center;
  border-radius: 4px;
  right: 200px;
  width: 290px;
  top: ${({top} : IToast) => top ? top: '20' }px;
  text-align: center;
  color: ${({ theme }) => (theme === "success" ? "rgb(139,193,185)" : "#E44126")};
  position: fixed;
  font-family: Effra;
  padding: 10px;
  z-index: 9999;
  @media(max-width: 900px) {
    right: 10px; 
  }
`;
const IconImg = styled.img`
  height: 30px;
  margin-right: 15px;
`;
const CloseImg = styled.img`
  margin-left: 10px;
  margin-top: 25px;
  cursor: pointer;
  padding-right: 20px;
`

const Toast: React.FC<IToast> = (props) => {
  const { setShowToast } = useErrors();
  const { message, theme, top } = props;
  return (
    <>
      {theme === "success" ? (
        <ContainerToast top={top} theme={theme}>
          <IconImg src={SuccessImg} alt=""></IconImg>
          <h5>{message}</h5>
          <CloseImg  onClick={() => setShowToast(false)} src={CloseSuccessPng} alt="" width="13" height="13"></CloseImg>
        </ContainerToast>
      ) : (
        <ContainerToast top={top} theme={theme}>
          <IconImg src={ErroImg} alt=""></IconImg>
          <h5>{message}</h5>
          <CloseImg  onClick={() => setShowToast(false)} src={CloseErrorPng} alt="" width="13" height="13"></CloseImg>
        </ContainerToast>
      )}
    </>
  );
};
export default Toast;
