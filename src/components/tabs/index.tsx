import styled from "styled-components";
import React, { useState } from "react";

interface ITab {
    header: string[];
}
interface IButtonEdit {
    active: boolean;
}

const TabsContent = styled.div`
    width: 100%;
    background-color: #fff;
`;
const Header = styled.div`
    width: 100%;
    display: flex;
    background-color: red;
`
const ButtonEdit = styled.button`
    padding: 15px 30px;
    flex-grow: 1;
    border: none;
    outline: none;
    cursor: pointer;
    background-color: ${(props: IButtonEdit) => props.active ? 'rgb(61, 61, 61)' : '#fff' };
    color: ${(props: IButtonEdit) => props.active ? '#fff' : 'rgb(61, 61, 61)' };
    border-bottom: ${(props: IButtonEdit) => props.active && '3px solid #ffb8b8' }; 
    transition-property: background-color;
    transition-duration: 0.4s;
`;
const ContentPage = styled.div`
    padding: 10px 10px;
`

const Tabs: React.FC<ITab> = ({children, ...props}) => {
    const { header } = props
    const childrens = React.Children.toArray(children)

    const [page, setPage] = useState(header[0])
    const [content, setContent] = useState(childrens[0])

    const onClick = (index: number) => {
        setPage(header[index])
        setContent(childrens[index])
    }

    return(
        <TabsContent>
            <Header>
                { header.map((item, index) => 
                    <ButtonEdit key={index} 
                        onClick={() => onClick(index)} 
                        active={item === page}
                    >
                        {item}
                    </ButtonEdit>
                )}
            </Header>
            <ContentPage>
                { content }
            </ContentPage> 
        </TabsContent>
    )
};

export default Tabs;