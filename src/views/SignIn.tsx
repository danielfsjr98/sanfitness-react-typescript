import styled from "styled-components";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Form from "../components/form/Form";
import Input from "../components/input/Input";
import { useAuth } from "../context/auth/index";
import { ILogin } from "../context/auth/interfaces"

const SignInContent = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    height: 100%;
    margin-right: auto;
    margin-left: auto;
`;

const ButtonStyle = styled.button`
    padding: 6px;
`

const CardLogin = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #ffffff;
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
    padding: 20px;
    min-height: 350px;
    min-width: 350px;
    border-radius: 5px;
    justify-self: center;
    @media(max-width: 900px) {
        min-height: 75%;
        min-width: 85vw;
    }
`

const SignIn: React.FC = (props) => {
    const { signIn, signed } = useAuth();
    const history = useHistory();
    const [login, setLogin] = useState<ILogin>({email: "", password: ""} as ILogin)

    useEffect(() => {
        if (signed) history.push('/home');   
    }, [signed, history]);
    
    const onSubmit = async (event: React.FormEvent) => {
        event.preventDefault();
        const payload = {
            email: login.email,
            password: login.password
        }
        await signIn(payload)
    };
    const onChangeEmail = async (event: React.FormEvent<HTMLInputElement>) => {
        setLogin({...login, email: event.currentTarget.value})
    };
    const onChangePassword = async (event: React.FormEvent<HTMLInputElement>) => {
        setLogin({...login, password: event.currentTarget.value})
    };
    
    return(
        <SignInContent>
            <CardLogin>
                <Form title="Contato" onSubmit={onSubmit}>
                    <Input value={login.email} 
                    onChange={onChangeEmail}
                    label="E-mail" placeholder="Insira seu email..."
                    />
                    <Input value={login.password} 
                    label="Senha" 
                    onChange={onChangePassword}
                    type="password"
                    placeholder="Insira sua senha..." />

                    <ButtonStyle type="submit"> Logar </ButtonStyle>
                </Form>
            </CardLogin>
        </SignInContent>
    )};

export default SignIn;