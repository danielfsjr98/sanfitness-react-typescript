import React, { useState } from "react";
import styled from "styled-components";
import Modal from "../../../components/modal"
import Input from "../../../components/input/Input"
import Button from "../../../components/button"
import Checkbox from "../../../components/checkbox"
import { useShop } from "../../../context/shop";
import { ISize, ICreateProduct } from "../../../context/shop/interfaces";

interface IModalProduct {
    title: string;
    onClickClose?: () => void;
}
interface ICheckSize {
    id: number;
    checked: boolean;
    amount: string;
}

const ContentModal = styled.div `
    display: flex;
`
const Header = styled.div`
    display: flex;
    font-size: 20px;
`
const Content = styled.div`
    display: flex;
    flex-direction: column;
    font-size: 20px;
`
const ContentSizes = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    width: 100%;
    margin-top: 6px;
    
`
const Column = styled.div`
    display: flex;
    flex-direction: column;
    flex-basis: 100%;
    flex: 1;
`
const Row = styled.div`
    display: flex;
    flex-direction: row;

    justify-content: space-between;
    align-items: flex-end;
    margin-bottom: 11px;
; 
`

const ModalProduct: React.FC<IModalProduct> = props => {
    const { newProduct } = useShop()
    const { title, onClickClose } = props
    const [product, setProduct] = useState<ICreateProduct>({} as ICreateProduct);
    const [sizePP, setSizePP] = useState({id: 1, checked: false, amount: "0"})
    const [sizeP, setSizeP] = useState({id: 2, checked: false, amount: "0"})
    const [sizeM, setSizeM] = useState({id: 3, checked: false, amount: "0"})
    const [sizeG, setSizeG] = useState({id: 4, checked: false, amount: "0"})
    const [sizeGG, setSizeGG] = useState({id: 5, checked: false, amount: "0"})
    const [sizeXG, setSizeXG] = useState<ICheckSize>({id: 6, checked: false, amount: "0"})
    const [sizes, setSizes] = useState<Array<ISize>>([])

    const onChangeChecked = (event: React.FormEvent<HTMLInputElement>) => {
        const { checked, id } = event.currentTarget
        const intId = parseInt(id)
        switch(intId){
            case 1: 
                handleSizes({checked, id: intId, amount: sizePP.amount});
                return setSizePP({checked, id: intId, amount: sizePP.amount});
            case 2: 
                handleSizes({checked, id: intId, amount: sizeP.amount});
                return setSizeP({checked, id: intId, amount: sizeP.amount});
            case 3: 
                handleSizes({checked, id: intId, amount: sizeM.amount});
                return setSizeM({checked, id: intId, amount: sizeM.amount});
            case 4: 
                handleSizes({checked, id: intId, amount: sizeG.amount});
                return setSizeG({checked, id: intId, amount: sizeG.amount});
            case 5: 
                handleSizes({checked, id: intId, amount: sizeGG.amount});
                return setSizeGG({checked, id: intId, amount: sizeGG.amount});
            case 6: 
                handleSizes({checked, id: intId, amount: sizeXG.amount});
                return setSizeXG({checked, id: intId, amount: sizeXG.amount});
            default:
                return;
        }
    }
    
    const onChangeInput = (event: React.FormEvent<HTMLInputElement>) => {
        const { name, value } = event.currentTarget
        switch(name){
            case 'PP': 
                handleSizes({ ...sizePP, amount: value });
                return setSizePP({ ...sizePP, amount: value });
            case 'P': 
                handleSizes({ ...sizeP, amount: value });
                return setSizeP({ ...sizeP, amount: value });
            case 'M': 
                handleSizes({ ...sizeM, amount: value });
                return setSizeM({ ...sizeM, amount: value });
            case 'G': 
                handleSizes({ ...sizeG, amount: value });
                return setSizeG({ ...sizeG, amount: value });
            case 'GG': 
                handleSizes({ ...sizeGG, amount: value });
                return setSizeGG({ ...sizeGG, amount: value });
            case 'XG': 
                handleSizes({ ...sizeXG, amount: value });
                return setSizeXG({ ...sizeXG, amount: value });
            default:
                return;
        }
    }

    const handleSizes = (size: ICheckSize) => {
            const checkedSizes = [...sizes];
            const index = checkedSizes.findIndex(el => el.id === size.id);
            const newValue = {id: size.id, quantityInStock: size.amount}

            if(index !== -1 && !size.checked) checkedSizes.splice(index, 1);
            if(index !== -1 && size.checked) checkedSizes[index] = newValue;
            if (index === -1 && size.checked) checkedSizes.push(newValue);

            const filteredSizes = checkedSizes.filter(el => parseInt(el.quantityInStock) > 0)
            return setSizes(filteredSizes);
    }

    const createProduct = async () => {
        if(sizes.length > 0){
            const payload: ICreateProduct = {
                imageUrl: product.imageUrl,
                name: product.name,
                price: product.price,
                ref: product.ref,
                description: product.description,
                sizes
            }
            await newProduct(payload);
            //O BACK END ESPERA UM DENTRO DO ARRAY DE SIZE size_id
            console.log("cadastrado");
        }
    }

    const onChangeName = (event: React.FormEvent<HTMLInputElement>) => setProduct({...product, name: event.currentTarget.value});
    const onChangeRef = (event: React.FormEvent<HTMLInputElement>) => setProduct({...product, ref: event.currentTarget.value});
    const onChangePrice = (event: React.FormEvent<HTMLInputElement>) => setProduct({...product, price: event.currentTarget.value});
    const onChangeDescription = (event: React.FormEvent<HTMLInputElement>) => setProduct({...product, description: event.currentTarget.value});
    const onChangeImage = (event: React.FormEvent<HTMLInputElement>) => setProduct({...product, imageUrl: event.currentTarget.value});

    return (
        <ContentModal>
            <Modal>
                <Header>
                    { title }
                </Header>
                <Content>
                    <Input label="Nome" value={product.name} onChange={onChangeName} />
                    <Input label="Referência" value={product.ref} onChange={onChangeRef} />
                    <ContentSizes>
                        <Column>
                            <Row>
                                <Checkbox label="PP" checked={ sizePP.checked } id={sizePP.id}  onChange={onChangeChecked}/>
                                { sizePP.checked &&
                                    <div style={{maxWidth: '65%', paddingRight: '15%'}}>
                                        <Input name="PP" value={sizePP.amount} label="Quantidade" type="number" padding="4px" onChange={onChangeInput} />
                                    </div> 
                                   
                                }
                            </Row>
                            <Row>
                                <Checkbox label="M" checked={ sizeM.checked } id={sizeM.id} onChange={onChangeChecked} />
                                { sizeM.checked &&
                                    <div style={{maxWidth: '65%', paddingRight: '15%'}}>
                                        <Input name="M" value={sizeM.amount} label="Quantidade" type="number" padding="4px" onChange={onChangeInput} /> 
                                    </div>
                                }
                            </Row>
                            <Row>
                                <Checkbox label="GG" checked={ sizeGG.checked } id={sizeGG.id} onChange={onChangeChecked} /> 
                                { sizeGG.checked && 
                                    <div style={{maxWidth: '65%', paddingRight: '15%'}}>
                                        <Input name="GG" value={sizeGG.amount} label="Quantidade" type="number" padding="4px" onChange={onChangeInput} /> 
                                    </div>
                                }
                            </Row>
                        </Column>
                        <Column>
                            <Row>
                                <Checkbox label="P" checked={ sizeP.checked } id={sizeP.id} onChange={onChangeChecked} />
                                { sizeP.checked &&
                                    <div style={{maxWidth: '65%', paddingRight: '15%'}}>
                                        <Input name="P" value={sizeP.amount} label="Quantidade" type="number" padding="4px" onChange={onChangeInput} /> 
                                    </div>
                                }
                            </Row>
                            <Row>
                                <Checkbox label="G" checked={ sizeG.checked } id={sizeG.id} onChange={onChangeChecked} />
                                { sizeG.checked && 
                                    <div style={{maxWidth: '65%', paddingRight: '15%'}}>
                                        <Input name="G" value={sizeG.amount} label="Quantidade" type="number" padding="4px" onChange={onChangeInput} /> 
                                    </div>
                                }
                            </Row>
                            <Row>
                                <Checkbox label="XG" checked={ sizeXG.checked } id={sizeXG.id} onChange={onChangeChecked} />
                                { sizeXG.checked &&
                                    <div style={{maxWidth: '65%', paddingRight: '15%'}}>
                                        <Input name="XG" value={sizeXG.amount} label="Quantidade" type="number" padding="4px" onChange={onChangeInput} /> 
                                    </div>
                                }
                            </Row>
                        </Column>
                    </ContentSizes>
                    <Input label="Preço" value={product.price} onChange={onChangePrice} />
                    <Input label="Descrição" value={product.description} onChange={onChangeDescription} />
                    <Input label="URL da imagem" value={product.imageUrl} onChange={onChangeImage} />
                    <div style={{marginTop: "10px"}} />
                    <Button text="Confirmar" onClick={createProduct} />
                    <div style={{marginTop: "8px"}} />
                    <Button text="Cancelar" onClick={onClickClose} />
                </Content>
            </Modal>
        </ContentModal>
    )
};

export default ModalProduct;
