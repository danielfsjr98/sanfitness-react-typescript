import styled from "styled-components";
import React, { useEffect, useState } from "react";
import { useShop } from "../../../context/shop"
import Table from "../../../components/table/Index"
import {formatCurrency} from "../../../utils"
import { useHistory } from "react-router-dom";
import Button from "../../../components/button";
import Modal from "./ModalProduct"
import { transpileModule } from "typescript";

interface IDataTable {
    ref: string;
    name: string;
    price: string;
} 

const ProductStyle = styled.div`
    display: flex;
    flex-direction: column;
    min-width: 100%;
    @media(max-width: 800px){
        min-width: 100%;
    }
`;

const ContentButtonAdd = styled.div`
    width: 30%;
    align-self: flex-end;
    margin-bottom: 10px;
    @media(max-width: 800px){
        width: 55%;
    }
`;

const Product: React.FC = (props) => {
    const history = useHistory();
    const { getAvailableProductsShop, productsAvailableShop } = useShop()
    // const [ name, setName ] = useState("")
    const [ data, setData ] = useState<Array<IDataTable>>([])
    const [ showModal, setShowModal ] = useState(false)

    useEffect(() => {
        getAvailableProductsShop();
    }, [getAvailableProductsShop])

    useEffect(() => {
        setData(productsAvailableShop.map(product => {
            return { 
                ref: product.ref, 
                name: product.name, 
                price: formatCurrency(product.price) 
            }
        }))
    }, [productsAvailableShop])

    const onClick = (ref: string) => {
        history.push(`/product/${ref}`);
    }

    const closeModal = () => setShowModal(false)
    const openModal = () => setShowModal(true)

    return(
        <ProductStyle> 
            { showModal && <Modal title="Adicionar produto" onClickClose={closeModal} /> }
            <ContentButtonAdd>
                <Button text="Cadastrar produto" onClick={openModal} />
            </ContentButtonAdd>
            <Table
                header={['REF', 'Nome', 'Preço']}
                items={data}
                action={true}
                onClick={onClick}
                keyOnClick="ref"
            />
        </ProductStyle>
    )
};
export default Product;