import styled from "styled-components";
import React from "react";
import Tabs from "../../components/tabs"
import Product from "./Product/Product"

const AdminContent = styled.div`
    width: 100%;
`;

const Admin: React.FC = (props) => {
    return(
        <AdminContent>
            <Tabs header={['Produtos', 'Pedidos', 'Usuários',]} >
                <Product />
                <div>Pedidos</div>
                <div>Usuários</div>
            </Tabs>
        </AdminContent>
    )
};

export default Admin;