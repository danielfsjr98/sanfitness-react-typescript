import styled from "styled-components";
import React from "react";
import { useAuth } from "../context/auth"

const HomeStyle = styled.div`
  min-width: 100%;
  @media(max-width: 800px){
      min-width: 100%;
  }
`;

const Home: React.FC = (props) => {
    const { userLogged }= useAuth()

    return(
        <HomeStyle> 
            <h2>Bem vindo, { userLogged && userLogged.name }</h2>  
        </HomeStyle>
    )
};
export default Home;