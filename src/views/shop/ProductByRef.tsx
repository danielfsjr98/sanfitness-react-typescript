import styled from "styled-components";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useShop } from "../../context/shop"
import { IProduct } from "../../context/shop/interfaces"
import Input from "../../components/input/Input"
import Select from "../../components/select"
import Button from "../../components/button"
interface IOption {
    name: string;
    id: string | number;
}

const PageContent = styled.div`
    max-width: 100%;
    margin: auto;
`
const ContentProductDetail = styled.div`
    max-width: 100%;
    display: flex;
    background-color: #fff;
    border-radius: 5px;
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
    @media(max-width: 768px){
        flex-direction: column;
    }
`
const ImgProduct = styled.div`
    display: flex;
    flex-direction: column;
    width: 50%;
    @media(max-width: 768px){
        width: 100%;

        img {
            width: 100%;
            max-height: 350px;
        }
    }
`
const ContentData = styled.div`
    display: flex;
    flex-direction: column;
    padding: 10px 20px;
    color: #3d3d3d;
    h2 {
        font-weight: 500;
    }
    h4 {
        font-weight: normal;
    }

`

const ContentButtonCart = styled.div`
    width: 45%;
    min-width: 200px;
    @media(max-width: 768px){
        width: 100%;
        min-width: 100%;
    }
`

const ProductByRef: React.FC = () => {
    const { getProductByRef, productsByRef } = useShop()
    const history = useHistory();
    const [id, setId] = useState("")
    const [productsAvailable, setProductsAvailable] = useState<Array<IProduct>>([] as Array<IProduct>)
    const [currentProduct, setCurrentProduct] = useState<IProduct>(productsAvailable[0])
    const [sizes, setSizes] = useState<Array<IOption>>([])
    const [selectedOption, setSelectedOption] = useState<IOption>({} as IOption)

    useEffect(() => { 
        setProductsAvailable(productsByRef);
        setCurrentProduct(productsByRef[0]);
        if(productsByRef[0]){
            setSelectedOption({
                id: productsByRef[0].id,
                name: productsByRef[0].sizeName
            })
        }
        const availableSizes = productsByRef.map(product => {
           return {id: product.id, name: product.sizeName}
        })
        if(availableSizes.length > 0) setSizes(availableSizes)
    }, [productsByRef]);
    
    useEffect(() => {
        setId(history.location.pathname.split("/")[2]);
        if(id) getProductByRef(id);
    }, [id]);

    useEffect(() => {
        changeProduct();
    }, [selectedOption]);

    const changeProduct = () => {
        const product = productsAvailable.find(product => product.id === selectedOption.id);
        product && setCurrentProduct(product);
    }

    const backToProducts = () => {
        history.push('/products')
    }

    return(
        <PageContent>
            { currentProduct &&
                <ContentProductDetail>
                    <ImgProduct>
                        <img src={currentProduct.imageUrl} alt="product"/>
                    </ImgProduct>
                    <ContentData>
                        <h4>{ currentProduct.ref }</h4>
                        <h2>{ currentProduct.name }</h2>
                        <div>Quantidade disponível: { currentProduct.quantityInStock }</div>                           
                        <Select label="Tamanho" 
                            items={sizes}
                            setSelectedValue={setSelectedOption}
                        />
                        <Input type="number" label="Quantidade" />
                        <ContentButtonCart>
                            <Button 
                                color="#3d3d3d"
                                text="Adicionar ao carrinho"
                                background="#ffb8b8"
                            />
                        </ContentButtonCart>
                        <div style={{margin: '10px 0'}}>{ currentProduct.description }</div>
                        <div style={{width: '30%'}}>
                            <Button 
                                text="Voltar" 
                                onClick={backToProducts} 
                                color="#3d3d3d"
                            />
                        </div>
                    </ContentData>
                </ContentProductDetail>
            }
        </PageContent>
    )

};

export default ProductByRef;