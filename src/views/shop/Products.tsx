import styled from "styled-components";
import React, { useEffect } from "react";
import { useShop } from "../../context/shop"
import CardProduct from "../../components/cardProduct"

const ProductsStyle = styled.div`
    width: 100%;
    display: grid;
    padding: 0 30px;
    grid-gap: 1rem;
    grid-auto-rows: 400px;

    @media(max-width: 768px){
      padding: 10px 18px;
    }
    @media(min-width: 400px) {
        grid-template-columns: repeat(1, 1fr); 
    }
    @media(min-width: 600px) {
        grid-template-columns: repeat(2, 1fr); 
    }
    @media(min-width: 900px) {
        grid-template-columns: repeat(3, 1fr);
    }
    @media(min-width: 1200px) {
        grid-template-columns: repeat(4, 1fr);
    }
    @media(min-width: 1400px) {
        grid-template-columns: repeat(5, 1fr);
    }
`

const Contact: React.FC = (props) => {

    const { getAvailableProductsShop, productsAvailableShop } = useShop()

    useEffect(() => {
        getAvailableProductsShop()
    }, [])

    return(
        <ProductsStyle>
            { productsAvailableShop.map(product => 
                <CardProduct key={product.id} product={product} />
            )}
        </ProductsStyle>
    )
};

export default Contact;