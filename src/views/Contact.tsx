import styled from "styled-components";
import React from "react";

import Input from "../components/input/Input";

const ContactStyle = styled.div`
  
`;

const Contact: React.FC = (props) => {

    return(
        <ContactStyle>
            <Input label="Nome" placeholder="Insira seu nome..."/>
            <Input label="E-mail" placeholder="Insira seu e-mail..." />
            <Input label="CPF" type="number" placeholder="Insira seu CPF" />
        </ContactStyle>
    )
};

export default Contact;