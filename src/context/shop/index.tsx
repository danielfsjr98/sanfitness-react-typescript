import React, {
  createContext,
  useCallback,
  useContext,
  useState
} from "react";
  
import {
    IProductsData,
    IProduct,
    ICreateProduct
} from "./interfaces";

import productsService from "../../services/products";
import { useErrors } from "../error";
import { useHistory } from "react-router-dom";

const ShopContext = createContext<IProductsData>({} as IProductsData);

export const ShopProvider: React.FC = ({ children }) => {
  const { handlerError, setToast, setThemeToast, clearError } = useErrors();
  const [productsAvailableShop, setProductsAvailableShop] = useState<Array<IProduct>>([])
  const [productsByRef, setProductsByRef] = useState<Array<IProduct>>([] as Array<IProduct>)
  const history = useHistory();

  const getAvailableProductsShop = useCallback(
    async () => {
      try {
        const { data } = await productsService.getAvailableProductsShop();
        setProductsAvailableShop(data)
        return data;
      } catch (error) {
        return false;
      }
    }, []
  )

  const getProductByRef = useCallback(
    async (ref: string) => {
      try {
        const { data } = await productsService.getProductByRef(ref);
        setProductsByRef(data.reverse())
        return data;
      } catch (error) {
        setToast(error.response.data);
        setThemeToast("danger");
        handlerError(error);
        if(error.response.data === 'Produto não encontrado.') history.push('/products')
      }
    },[]
  )

  const newProduct = useCallback(
    async (payload: ICreateProduct) => {
      try {
        const { data } = await productsService.createProducts(payload);
        return data;
      } catch (error) {
        return false;
      }
    }, []
  )

  return (
    <ShopContext.Provider
      value={{
        productsAvailableShop,
        getAvailableProductsShop,
        newProduct,
        getProductByRef,
        productsByRef
      }}
    >
      {children}
    </ShopContext.Provider>
  );
};

export const useShop = () => {
  const context = useContext(ShopContext);
  return context;
};

export default ShopContext;