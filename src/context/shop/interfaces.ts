export interface IProductsData {
    productsAvailableShop: IProduct[],
    getAvailableProductsShop(): Promise<void>;
    getProductByRef(ref: string): Promise<void>;
    newProduct(payload: ICreateProduct): Promise<void>;
    productsByRef: Array<IProduct>;
}
export interface IProduct {
    id: number;
    ref: string;
    name: string;
    price: string;
    quantityInStock?: number;
    imageUrl: string;
    size_id?: number;
    description: string;
    sizes?: Array<IProductSizes>;
    sizeName: string;
}

export interface IProductSizes {
    idSize: number;
    quantityInStock: string; 
}

export interface ISize {
    id: number;
    quantityInStock: string;
}
export interface ICreateProduct {
    imageUrl: string;
    name: string,
    price: string,
    description: string,
    ref: string,
    sizes: Array<ISize>
}
