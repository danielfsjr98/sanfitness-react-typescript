import { AxiosError } from "axios";

export interface IErrorsData {
  handlerError: (error: AxiosError, ignoreProperty?: boolean, signOut?: () => void) => void;
  restoreValues: () => void;
  alterErrorMessage: (payload: string) => void;
  alterErrorProperty: (payload: string) => void;
  toastVisibility: (payload: boolean) => void;
  clearError: () => void;
  setThemeToast: React.Dispatch<React.SetStateAction<string>>;
  setShowToast: React.Dispatch<React.SetStateAction<boolean>>;
  setMessageToast: React.Dispatch<React.SetStateAction<string>>;
  setToast: (message: string, theme?: string) => void;
  setReturnUrl:React.Dispatch<React.SetStateAction<string>>,
  returnUrl: string,
  messageToast: string;
  errorMessage: string;
  errorProperty: string,
  themeToast: string;
  showToast: boolean;
}
