import React, {
    createContext,
    useState,
    useContext,
  } from "react";
  import { AxiosError } from "axios";
  import { IErrorsData } from "./interfaces";
  import { useHistory } from "react-router-dom";
  
  const ErrorsContext = createContext<IErrorsData>({} as IErrorsData);
  
  export const ErrorProvider: React.FC = ({ children }) => {
    const history = useHistory();
  
    const [errorMessage, setErrorMessage] = useState<string>("");
    const [errorProperty, setErrorProperty] = useState<string>("");
    const [showToast, setShowToast] = useState<boolean>(false);
    const [themeToast, setThemeToast] = useState<string>("");
    const [messageToast, setMessageToast] = useState<string>("");
    const [returnUrl, setReturnUrl] = useState<string>("");
  
    const setTimeClearError = () => {
      setTimeout(() => {
        setShowToast(false);
      }, 5000);
    };
    const setToast = (message: string, theme: string = "success") => {
      setMessageToast(message);
      setShowToast(true);
      setThemeToast(theme);
      setTimeClearError();
    };
    const clearError = () => {
      setErrorMessage("");
      setErrorProperty("");
      setTimeClearError();
    };
  
    const handlerError = (error: AxiosError, ignoreProperty = false) => {
      if (error.response?.status === 403 || error.response?.status === 401) {
        const path = history.location.pathname;
        if (path !== "/login") {
          setReturnUrl(path);
        }
        return;
      }
      if (ignoreProperty) {
        setMessageToast(error.response?.data);
        setShowToast(true);
        setThemeToast("danger");
        setTimeClearError();
        return;
      }
      const property = error.response?.data;
      if (property) {
        setErrorMessage(error.response?.data);
        setErrorProperty(property);
      }
      if (!property) {
        setMessageToast(error.response?.data);
        setShowToast(true);
        setThemeToast("danger");
        setTimeClearError();
      }
    };
    const restoreValues = () => {
      setErrorMessage("");
      setErrorProperty("");
      setMessageToast("");
    };
    const alterErrorMessage = (payload: string) => {
      setErrorMessage(payload);
    };
    const alterErrorProperty = (payload: string) => {
      setErrorProperty(payload);
    };
    const toastVisibility = (payload: boolean) => {
      setShowToast(payload);
    };
  
    return (
      <ErrorsContext.Provider
        value={{
          handlerError,
          restoreValues,
          alterErrorMessage,
          alterErrorProperty,
          toastVisibility,
          setShowToast,
          clearError,
          setMessageToast,
          setThemeToast,
          setToast,
          setReturnUrl,
          returnUrl,
          themeToast,
          errorMessage,
          errorProperty,
          showToast,
          messageToast,
        }}
      >
        {children}
      </ErrorsContext.Provider>
    );
  };
  
  export const useErrors = () => {
    const context = useContext(ErrorsContext);
    return context;
  };
  export default ErrorsContext;
  