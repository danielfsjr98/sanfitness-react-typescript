export interface IAuthContextData {
    signed: boolean;
    signIn(payload: ILogin): Promise<void>;
    validateSession(): void;
    signout(): void;
    userLogged: IUser;
}

export interface ILogin {
    email: string;
    password: string;
}

export interface IUser {
    admin: Boolean;
    adress: String;
    cep: String;
    cpf: String;
    email: String
    exp: Number
    iat: Number
    id: Number
    name: String
    reseller: Boolean
    token: String
}