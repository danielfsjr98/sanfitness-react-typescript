import React, { createContext, useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import { IAuthContextData, ILogin, IUser } from "./interfaces";
import authService from "../../services/auth";
import { useErrors } from "../error";

const AuthContext = createContext<IAuthContextData>({} as IAuthContextData);

export const AuthProvider: React.FC = ({ children }) => {
  const { handlerError, setToast, setThemeToast, clearError } = useErrors();
  const [signed, setSigned] = useState(true);
  const [userLogged, setUserLogged] = useState<IUser>({} as IUser);
  const history = useHistory();

  const signIn = async (payload: ILogin) => {
    clearError();
    try {
      const { data } = await authService.signin(payload)
      localStorage.setItem("sanfitness:token", JSON.stringify(data));
      setSigned(true);
      setToast("Login realizado.");
      return data
    } catch (error) {
      setToast(error.response.data);
      setThemeToast("danger");
      handlerError(error);
    }
  };

  const validateSession = async () => {
    try {
      const json = localStorage.getItem("sanfitness:token");
      const userData = json && JSON.parse(json);
      if(!userData) {
        setSigned(false);
        return history.push("/login");
      } else {
        const { data } = await authService.setValidateToken(userData);
        setSigned(data);
        if(!data) return history.push("/login");
        authService.setUser(userData);
        setUserLogged(userData);
      }
    }
    catch(error) {
      setToast(error.response.data);
      setThemeToast("danger");
      handlerError(error);
    }
  }

  const signout = () => {
    localStorage.removeItem("sanfitness:token");
    setSigned(false);
    setUserLogged({} as IUser);
  };

  return (
    <AuthContext.Provider
      value={{
        signed,
        signIn,
        validateSession,
        signout,
        userLogged
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  const context = useContext(AuthContext);
  return context;
};

export default AuthContext;